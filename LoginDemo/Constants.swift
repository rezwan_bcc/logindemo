//
//  Constants.swift
//  LoginDemo
//
//  Created by Rezwan Islam on 5/22/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let id = "admin"
    static let password = "admin"
    static let errorText = "Wrong user ID and/or Password!"
    static let detailVCSegue = "detailVCSegue"
    
    static let actionTitleLike = "Like"
    static let actionTitleShare = "Share"
    static let actionTitleExit = "Exit"
    static let actionTitleCancel = "Cancel"
    static let actionSheetTitle = "Actions"
    static let actionSheetMessage = "Select an action."
}
