//
//  ViewController.swift
//  LoginDemo
//
//  Created by Rezwan Islam on 5/22/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginVC: UIViewController {

    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var idEmptyLabel: UILabel!
    @IBOutlet weak var passwordEmptyLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loginButton.layer.cornerRadius = 5
        loginView.layer.cornerRadius = 5
        SVProgressHUD.setFadeInAnimationDuration(0.0)
        SVProgressHUD.setFadeOutAnimationDuration(0.1)
        SVProgressHUD.setOffsetFromCenter(UIOffset(horizontal: 0, vertical: 0))
        
        let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tapGesture)
        setupTextFields()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        login()
    }
    
    fileprivate func setupTextFields() {
        idTextField.delegate = self
        idTextField.returnKeyType = .next
        idTextField.enablesReturnKeyAutomatically = true
        idTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        passwordTextField.delegate = self
        passwordTextField.returnKeyType = .go
        passwordTextField.enablesReturnKeyAutomatically = true
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    fileprivate func login() {
        if idTextField.text != Constants.id || passwordTextField.text != Constants.password{
            SVProgressHUD.showError(withStatus: Constants.errorText)
            SVProgressHUD.dismiss(withDelay: 1.5)
            return
        }
        performSegue(withIdentifier: Constants.detailVCSegue, sender: self)
    }
    
    func handleTap(_ tapGesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(idTextField) {
            idEmptyLabel.isHidden = true
        } else if textField.isEqual(passwordTextField) {
            passwordEmptyLabel.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)! {
            if textField.isEqual(idTextField) {
                idEmptyLabel.isHidden = false
            } else if textField.isEqual(passwordTextField) {
                passwordEmptyLabel.isHidden = false
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(idTextField) {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
            if !((idTextField.text?.isEmpty)! || (passwordTextField.text?.isEmpty)!) {
                login()
            }
        }
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if (idTextField.text?.isEmpty)! || (passwordTextField.text?.isEmpty)! {
            loginButton.isEnabled = false
            loginButton.alpha = 0.6
        } else {
            loginButton.isEnabled = true
            loginButton.alpha = 1.0
        }
    }
    
}

