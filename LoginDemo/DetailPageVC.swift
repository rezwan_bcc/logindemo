//
//  DetailPageVCViewController.swift
//  LoginDemo
//
//  Created by Rezwan Islam on 5/22/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import UIKit
import SVProgressHUD

class DetailPageVC: UIViewController {
    
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var backButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        actionButton.layer.cornerRadius = 5
        backButton.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func actionButtonPressed(_ sender: UIButton) {
        
        let likeAction = UIAlertAction(title: Constants.actionTitleLike, style: .default) { (action) in
            SVProgressHUD.showSuccess(withStatus: "Done")
            SVProgressHUD.dismiss(withDelay: 1.5)
        }
        let shareAction = UIAlertAction(title: Constants.actionTitleShare, style: .default) { (action) in
            SVProgressHUD.showSuccess(withStatus: "Done")
            SVProgressHUD.dismiss(withDelay: 1.5)
        }
        let exitAction = UIAlertAction(title: Constants.actionTitleExit, style: .destructive) { (action) in
            exit(0)
        }
        let cancelAction = UIAlertAction(title: Constants.actionTitleCancel, style: .cancel) { (action) in
        }
        
        let actionSheet = UIAlertController(title: Constants.actionSheetTitle, message: Constants.actionSheetMessage, preferredStyle: .actionSheet)
        actionSheet.addAction(likeAction)
        actionSheet.addAction(shareAction)
        actionSheet.addAction(exitAction)
        actionSheet.addAction(cancelAction)
        self.show(actionSheet, sender: self)
        
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}



















